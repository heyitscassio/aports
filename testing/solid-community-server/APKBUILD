# Maintainer: Dylan Van Assche <me@dylanvanassche.be>
# Contributor: Dylan Van Assche <me@dylanvanassche.be>
pkgname=solid-community-server
pkgver=1.0.0
pkgrel=0
pkgdesc="An open and modular implementation of the Solid specifications"
url="https://github.com/solid/community-server"
arch="all"
license="MIT"
depends="nodejs>=12"
makedepends="npm build-base python3"
subpackages="$pkgname-openrc"
install="$pkgname.pre-install"
source="$pkgname-$pkgver.tar.gz::https://github.com/solid/community-server/archive/refs/tags/v$pkgver.tar.gz
	solid-community-server.initd"
builddir="$srcdir/community-server-$pkgver"
options="!check textrels" # tests fail on musl

build() {
	ls
	npm install --include=dev
	npm run build
}

check() {
	npm run test
}

package() {
	mkdir -p "$pkgdir"/usr/share/webapps/solid-community-server
	mkdir -p "$pkgdir"/usr/bin

	cp -r "$builddir"/package.json \
		"$builddir"/bin \
		"$builddir"/config \
		"$builddir"/dist \
		"$builddir"/node_modules \
		"$builddir"/templates \
		"$pkgdir"/usr/share/webapps/solid-community-server

	ln -s /usr/share/webapps/solid-community-server/bin/server.js \
		"$pkgdir"/usr/bin/solid-community-server

	install -m755 -D "$srcdir"/$pkgname.initd \
		"$pkgdir"/etc/init.d/$pkgname
}

sha512sums="
e3febf67f01a00bd3fcdda106019db94dcb3e617d7d780d49499cebcbfe2948bc174a4f2f9c76971c30815e25b5ad8d829ea542c89171fe84927d7aaefdd5e42  solid-community-server-1.0.0.tar.gz
5d48353a2ee00411967dccbe37fa140134f734cf0f87fa04b489a05978b9a1e1ec8908ff003c5e25523345943672dde1eeb5e0b559d96407c67eb7f287601a53  solid-community-server.initd
"
